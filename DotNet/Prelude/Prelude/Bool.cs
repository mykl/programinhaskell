﻿

namespace Prelude
{
    public struct Bool : IEq<Bool>
    {
        private readonly bool value;
        public static readonly Bool True = new Bool(true);
        public static readonly Bool False = new Bool(false);
        
        private Bool(bool value)
        {
            this.value = value;
        }
        public F<Bool, Bool> Eq
        {
            get
            {
                Bool capture = this;
                return x => x(Unit.Instance).value == capture.value ? True : False;
            }
        }

        public static bool operator ==(Bool a, Bool b)
        {
            return a.value == b.value;
        }

        public static bool operator !=(Bool a, Bool b)
        {
            return !(a == b);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Bool)) return false;
            return this == (Bool)obj;
        }
    }
}
