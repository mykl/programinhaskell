﻿using System;

namespace Prelude
{
    public class WriteOnce<T>
    {
        private T value;
        private bool written;

        public T Value
        {
            get
            {
                if (written) return value;
                throw new InvalidOperationException("No value written.");
            }
            set
            {
                if (written) throw new InvalidOperationException("Value already written.");
                this.value = value;
                this.written = true;
            }
        }

        public bool HasValue
        {
            get { return written; }
        }

        public override string ToString()
        {
            return written ? value.ToString() : "NO VALUE";
        }
    }

    
}
