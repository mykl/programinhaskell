﻿namespace Prelude
{
    public interface IEq<in T>
    {
        F<T, Bool> Eq { get; }
    }

    public interface INotEq<in T>
    {
        F<T, Bool> NotEq { get; }
    }

    public static class Eq<T> where T : IEq<T>
    {
        private static readonly Unit U = Unit.Instance;
        private static readonly bool ImplementsNotEq = typeof(INotEq<T>).IsAssignableFrom(typeof(T));

        public static readonly F<T, Bool> True = x => Bool.True;
        public new static readonly F<T, F<T, Bool>> Equals = x => y => x(U).Eq(y);

        public static readonly F<T, F<T, Bool>> NotEquals =
            x => y => ImplementsNotEq
                          ? ((INotEq<T>)x(U)).NotEq(y)
                          : ((IEq<T>)x(U)).Eq(y) == Bool.True ? Bool.False : Bool.True;
    }
}
