﻿namespace Prelude
{
    public interface  IEq<in T>
    {
        F<T, Bool> EqualTo { get; }
    }

    public static class Eq<T> where T : IEq<T>
    {
        private static readonly Unit U = Unit.Instance;
        public static readonly F<T, F<T, Bool>> EqualTo = x => y => x.EqualTo(y);
        public static readonly F<T, F<T, Bool>> NotEqualTo = x => y => x.EqualTo(y).Not;

        //public static readonly F<Lazy<T>, F<Lazy<T>, Lazy<Bool>>> Equal = x => y => x(U).Eq(y);
    }
}
