﻿namespace Prelude
{
    public interface IIntegral<T> : INum<T>
    {
        F<T, T> Div { get; }
        F<T, T> Mod { get; }

        Bool IsPositive { get;}

        Bool IsNegative { get; }
    }

    public static class Int<T> where T : IIntegral<T>, IOrd<T>
    {
        public static readonly T One = 1.ToIntegral<T>();
        public static readonly T Zero = 0.ToIntegral<T>();
        public static readonly T Two = 2.ToIntegral<T>();

        public static readonly F<T, F<T, T>> Div = x => y => x.Div(y);
        public static readonly F<T, F<T, T>> Mod = x => y => x.Mod(y);
        public static readonly F<T, Bool> Even = x => x.Mod(Two).EqualTo(Zero);
        public static readonly F<T, Bool> Odd = x => Even(x).Not;

        public static readonly F<T, F<T, T>> Exp =
            x => n =>
                     {
                         if (n.EqualTo(Zero)) return One;
                         if (n.LessThan(Zero)) throw new System.ArgumentException("n must be >= 0");
                         return x.Multiply(Exp(x)(n.Subtract(One)));
                     };

        public static F<TA, F<TB, TA>> GetExpFunc<TA, TB> ()
            where TA : INum<TA>
            where TB : IIntegral<TB>
        {
            F<TA, F<TB, TA>> exp = GetExpFunc<TA, TB> ();


            return x => n =>
            {
                if (n.EqualTo (0.ToIntegral<TB> ()))
                    return 1.ToNum<TA> ();
                if (n.IsNegative)
                    throw new System.ArgumentException ("n must be >= 0");


                return x.Multiply (exp (x) (n.Subtract (1.ToNum<TB> ())));
            };
        }
    }

    public static class Integral
    {
        public static T ToNum<T> (this int val) where T : INum<T>
        {
            if (typeof(T) == typeof(int))
                return (T)(object)(Int)val;
            throw new System.InvalidCastException ("cannot convert int to T");
        }
        public static T ToIntegral<T>(this int val) where T : IIntegral<T>
        {
            if (typeof(T) == typeof(int)) return (T)(object)(Int)val;
            throw new System.InvalidCastException("cannot convert int to T");
        }
    }

}
