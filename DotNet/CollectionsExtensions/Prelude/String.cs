﻿

namespace Prelude
{

    public static class S
    {
        // Convert CLS string to Prelude.String
        public static readonly F<string, List<Char>> Create =
            cs => cs == string.Empty
                      ? List<Char>.Nil
                      : List<Char>.Cons(cs[0])(Create(cs.Substring(1)));
    }
}
