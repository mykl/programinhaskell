﻿namespace Prelude
{
    public interface IOrd<in T> : IEq<T>
    {
        F<T, Bool> LessThan { get; }

        F<T, Bool> LessThanEqualTo { get; }

        F<T, Bool> GreaterThan { get; }

        F<T, Bool> GreaterThanEqualTo { get; }

    }

    public static class Ord<T> where T : IOrd<T>
    {
        public static readonly F<T, F<T, Bool>> LessThan = x => y => x.LessThan(y);
        public static readonly F<T, F<T, Bool>> LessThanEqualTo = x => y => x.LessThanEqualTo(y);
        public static readonly F<T, F<T, Bool>> GreaterThan = x => y => x.GreaterThan(y);
        public static readonly F<T, F<T, Bool>> GreaterThanEqualTo = x => y => x.GreaterThanEqualTo(y);
        public static readonly F<T, F<T, T>> Min = x => y => x.LessThanEqualTo(y) ? x : y;
        public static readonly F<T, F<T, T>> Max = x => y => x.LessThanEqualTo(y) ? x : y;
    }
}
