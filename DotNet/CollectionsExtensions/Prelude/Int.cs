﻿namespace Prelude
{
    using String = List<Char>;

    public struct Int : IIntegral<Int>
    {
        private readonly int val;
        private Int(int val)
        {
            this.val = val;
        }
        public static implicit operator Int(int val)
        {
            return new Int(val);
        }

        public Char CharValue
        {
            get { return (char)val; }
        }


        public F<Int, Bool> EqualTo
        {
            get 
            { 
                var capture = this;
                return x => capture.val == x.val;
            }
        }

        public F<Int, Bool> LessThan
        {
            get
            {
                var capture = this;
                return x => capture.val < x.val;
            }
        }

        public F<Int, Bool> LessThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val <= x.val;
            }
        }

        public F<Int, Bool> GreaterThan
        {
            get
            {
                var capture = this;
                return x => capture.val > x.val;
            }
        }

        public F<Int, Bool> GreaterThanEqualTo
        {
            get
            {
                var capture = this;
                return x => capture.val >= x.val;
            }
        }

        public String Show
        {
            get { return S.Create(val.ToString()); }
        }

        public F<Int, Int> Add
        {
            get
            {
                var capture = this;
                return x => capture.val + x.val;
            }
        }

        public F<Int, Int> Subtract
        {
            get
            {
                var capture = this;
                return x => capture.val - x.val;
            }
        }

        public F<Int, Int> Multiply
        {
            get
            {
                var capture = this;
                return x => capture.val * x.val;
            }
        }

        public Int Negate
        {
            get { return -val; }
        }

        public Int Abs
        {
            get { return val < 0 ? -val : val; }
        }

        public Int Signum
        {
            get { return val < 0 ? -1 : 1; }
        }

        public F<Int, Int> Div
        {
            get
            {
                var capture = this;
                return x => capture.val / x.val;
            }
        }

        public F<Int, Int> Mod
        {
            get
            {
                var capture = this;
                return x => capture.val % x.val;
            }
        }

        public Bool IsNegative
        {
            get { return val < 0; }
        }
        public Bool IsPositive
        {
            get { return val > 0; }
        }

        public static F<T, F<Int, T>> GetExpFunc<T> ()
            where T : INum<T>
        {
            F<T, F<Int, T>> exp = GetExpFunc<T> ();


            return x => n =>
            {
                if (n.EqualTo (0.ToIntegral<Int> ()))
                    return 1.ToNum<T> ();
                if (n.IsNegative)
                    throw new System.ArgumentException ("n must be >= 0");

                
                return x.Multiply (exp (x) (n.Subtract (1.ToNum<Int> ())));
            };
        }

    }

    public class IntReader : IRead<Int>
    {
        public F<String, Int> Read
        {
            get { throw new System.NotImplementedException(); }
        }
    }
}
