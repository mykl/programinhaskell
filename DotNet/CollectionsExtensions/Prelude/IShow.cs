﻿namespace Prelude
{
    using String = List<Char>;

    public interface IShow
    {
        String Show { get; }
    }

    public static class S<T> where T : IShow
    {
        public static readonly F<T, String> Show = t => t.Show;
    }
}
