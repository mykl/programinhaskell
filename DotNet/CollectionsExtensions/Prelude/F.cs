﻿namespace Prelude
{
    public delegate TResult F<in T, out TResult>(T arg1);

    public static class F<TA,TB,TC>
    {
        public static readonly F<F<TB, TC>, F<F<TA, TB>, F<TA, TC>>> Compose = f => g => x => f(g(x));
    }



    public static class F
    {
        public static F<TA,TC> Compose<TA,TB,TC>(this F<TB,TC> f, F<TA,TB> g)
        {
            return x => f(g(x));
        }

        public static F<F<TA,TB>,F<TA,TC>> Compose<TA,TB,TC>(this F<TB,TC> f)
        {
            return g => x => f(g(x));
        }

        public static F<TB,F<TA,TB>> Flip<TA,TB>(this F<TA,F<TB,TB>> f)
        {
            return x => y => f(y)(x);
        }

    }

    public delegate T Lazy<out T>(Unit _);

    public static class Const<TA,TB>
    {
        public static readonly F<TA, F<TB, TA>> Invoke = x => y => x;
    }
}
