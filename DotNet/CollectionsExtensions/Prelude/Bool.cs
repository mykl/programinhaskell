﻿namespace Prelude
{
    using String = List<Char>;

    public struct Bool : IOrd<Bool>, IShow
    {
        private readonly bool val;
        private Bool (bool val)
        {
            this.val = val;
        }

        public static readonly Bool True = new Bool(true);
        public static readonly Bool False = new Bool(false);
        public static readonly Bool Otherwise = True;

        public Bool Not
        {
            get
            {
                return val ? False : True;
            }
        }

        public F<Bool, Bool> EqualTo
        {
            get
            {
                var capture = this;
                return b => b.val == capture.val;
            }
        }

        public F<Bool, Bool> And
        {
            get
            {
                var capture = this;
                return b => capture.val ? b : False;
            }
        }

        public F<Bool, Bool> Or
        {
            get
            {
                var capture = this;
                return b => capture.val ? True : b;
            }
        }

        public F<Bool, Bool> LessThan
        {
            get
            {
                var capture = this;
                return b => capture.val ? False : b;
            }
        }
        public F<Bool, Bool> LessThanEqualTo
        {
            get
            {
                var capture = this;
                return b => capture.val ? b : True;
            }
        }
        public F<Bool, Bool> GreaterThan
        {
            get
            {
                var capture = this;
                return b => capture.val ? b.Not : False;
            }
        }
        public F<Bool, Bool> GreaterThanEqualTo
        {
            get
            {
                var capture = this;
                return b => capture.val ? True : b.Not;
            }
        }
        public String Show
        {
            get
            {
                return S.Create(val ? "True" : "False");
            }
        }


        #region .NET Integration

        public static implicit operator Bool (bool b)
        {
            return b ? True : False;
        }

        public static implicit operator bool (Bool b)
        {
            return b.val;
        }

        #endregion


    }

    /// <summary>
    /// Boolean functions. Name choice is to keep it short.
    /// </summary>
    public static class B
    {
        public static readonly F<Bool, Bool> Not = b => b.Not;
        public static readonly F<Bool, F<Bool, Bool>> And = x => y => x.And(y);
        public static readonly F<Bool, F<Bool, Bool>> Or = x => y => x.Or(y);
        public static readonly Bool Otherwise = Bool.Otherwise;
    }
    
}
