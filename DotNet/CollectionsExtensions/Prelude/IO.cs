﻿namespace Prelude
{
    public class IO<T>
    {
        private readonly T value;
        public IO(T value)
        {
            this.value = value;
        }

        public IO<TResult> Select<TResult>(System.Func<T,TResult> f)
        {
            return new IO<TResult>(f(value));
        }
    }
}


namespace Prelude
{
    using String = List<Char>;
    public static class PutStrLn<T> where T : IShow
    {
        public static readonly F<T, IO<Unit>> Invoke =
            xs =>
                {
                    System.Console.WriteLine(xs.Show);
                    return new IO<Unit>(Unit.Instance);
                };

    }

    public static class GetLine
    {
        public static IO<String> Invoke
        {
            get { return new IO<String>(S.Create(System.Console.ReadLine())); }
        }
                                             
    }
}
