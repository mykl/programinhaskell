﻿namespace Prelude
{
    public interface IFractional<T> : INum<T>
    {
        F<T, T> Div { get; }
        T Recip { get; }
    }

    public static class Frac<T> where T : IFractional<T>
    {
        public static readonly F<T, F<T, T>> Div = x => y => x.Div(y);
        public static readonly F<T, T> Recip = x => x.Recip;
    }
}
