﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CollectionsExtensions;

namespace TestIterator
{
    class Program
    {
        static void Main(string[] args)
        {



            var array = new int[] { 1, 3, 5, 7, 11, 13, 17, 19 };
            Console.Write(array.Show());

            var newList = EnumerableExtensions.Map(x => x * 2, array);
            Console.WriteLine(newList.Show());

            var list = 5.Iterate(x => x * 2);
            Console.WriteLine(list.Take(10).Show());


            var iterator = array.GetIterator();

            while(iterator.HasCurrent)
            {
                Console.WriteLine(iterator.Current);
                iterator.MoveNext();
            }

            iterator = Enumerable.Empty<int>().GetIterator();
            while (iterator.HasCurrent)
            {
                Console.WriteLine(iterator.Current);
                iterator.MoveNext();
            }
        }
    }
}
