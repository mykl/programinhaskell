﻿using System;

namespace CollectionsExtensions
{
    public static class FuncHelper
    {
        public static bool Not(bool b)
        {
            return !b;
        }

        public static readonly Func<bool, bool> NotFunc = Not;

        /// <summary>
        /// Always returns constVal
        /// </summary>
        public static T1 Const<T1, T2>(this T1 constVal, T2 _)
        {
            return constVal;
        }

        /// <summary>
        /// Returns a function that always returns constVal
        /// </summary>
        public static Func<TIgnore, TConst> Const<TIgnore,TConst>(this TConst constVal)
        {
            return _ => constVal;
        }

        /// <summary>
        /// A curried version of <see cref="Const{T1,T2}(T1,T2)"/>.
        /// </summary>
        public static Func<T1, Func<T2, T1>> GetConstFunc<T1, T2>()
        {
            return ((Func<T1, T2, T1>)Const).ToCurry();
        }

        /// <summary>
        /// Converts a 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="f"></param>
        /// <returns></returns>
        public static Predicate<T> ToPredicate<T>(this Func<T, bool> f)
        {
            return x => f(x);
        }

        /// <summary>
        /// Returns a function that produces the same result as f, but takes
        /// its arguments in reverse order.
        /// </summary>
        public static Func<T2, T1, TResult> Flip<T1,T2,TResult>(this Func<T1, T2, TResult> f)
        {
            return (y, x) => f(x, y);
        }

        /// <summary>
        /// A curried version of <see cref="Flip{T1,T2,TResult}(System.Func{T1,T2,TResult})"/>
        /// </summary>
        public static Func<Func<TA, Func<TB, TC>>, Func<TB, Func<TA, TC>>> GetFlipFunc<TA,TB,TC>()
        {
            return f => x => y => f(y)(x);
        }

        /// <summary>
        /// Returns a curried version of f
        /// </summary>
        public static Func<T1, Func<T2, TResult>> Curry<T1,T2,TResult>(Func<T1, T2, TResult> f)
        {
            return x => y => f(x, y);
        }

        /// <summary>
        /// Returns a curried version of f
        /// </summary>
        public static Func<T1, Func<T2, TResult>> ToCurry<T1, T2, TResult>(this Func<T1, T2, TResult> f)
        {
            return Curry(f);
        }

        /// <summary>
        /// Returns a curried version of f
        /// </summary>
        public static Func<T1, Func<T2, Func<T3, TResult>>> ToCurry<T1,T2,T3,TResult>(this Func<T1, T2, T3, TResult> f)
        {
            return x => y => z => f(x,y,z);
        }

        public static Func<T1, Func<T2, Func<T3, Func<T4, TResult>>>> ToCurry<T1, T2, T3, T4, TResult>(this Func<T1, T2, T3, T4, TResult> f)
        {
            return w => x => y => z => f(w, x, y, z);
        }

        public static Func<T1, T2, TResult> Uncurry<T1,T2,TResult>(Func<T1, Func<T2, TResult>> f)
        {
            return (x, y) => f(x)(y);
        }

        public static Func<T1, T2, T3, TResult> Uncurry<T1,T2,T3,TResult>(Func<T1, Func<T2, Func<T3, TResult>>> f)
        {
            return (x, y, z) => f(x)(y)(z);
        }

        public static Func<TA,TC> Compose<TA,TB,TC>(this Func<TB,TC> f, Func<TA,TB> g)
        {
            return x => f(g(x));
        }

        public static Func<Func<TB,TC>, Func<Func<TA,TB>,Func<TA,TC>>> GetComposeFunc<TA,TB,TC>()
        {
            Func<Func<TB, TC>, Func<TA, TB>, Func<TA, TC>> composeFunction = Compose;
            return composeFunction.ToCurry();
        }

        public static Func<T,T> GetIdFunc<T>()
        {
            return x => x;
        }
    }
}
