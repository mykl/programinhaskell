﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionsExtensions
{
    public class Program
    {
        

        public static void Main2()
        {


            var arr1 = Enumerable.Empty<int>();
            var arr2 = new[] { 1, 3, 5, 7 };
            var arr3 = new[] { "hello", "goodbye" };

            var result1 = arr1.Zip(arr2);
            var result2 = arr2.Zip(arr1);

            var result3 = arr2.Zip(arr3);
            var result4 = arr3.Zip(arr2);

            var result5 = arr1.Zip(arr3);
            var result6 = arr3.Zip(arr1);


            var array = new [] { 1, 3, 5, 7, 11, 13, 17, 19 }; 
            
            // ridiculous example - since this could be simplified
            // by using foreach and an enumerator.
            var iterator = array.GetIterator();

            while (iterator.HasCurrent)
            {
                Console.WriteLine(iterator.Current);
                iterator.MoveNext();
            }


            iterator = Enumerable.Empty<int>().GetIterator();
            while (iterator.HasCurrent)
            {
                Console.WriteLine(iterator.Current);
                iterator.MoveNext();
            }

            List<int> list = new List<int> { 1, 3, 4, 5, 7, 9 };
            Func<int, bool> isEvenFunc = x => x % 2 == 0;
            var index = list.FindIndex(isEvenFunc.ToPredicate());

            Func<int, bool> alwaysFalse = false.Const<int, bool>();

            var result = E<bool>.Empty(isEvenFunc.Map(array));
        }
    }
}