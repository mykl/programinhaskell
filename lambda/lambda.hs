iff True t _ = t
iff False _ f = f

z f x = x

o f x = f x

t f x = f (f x)

th f x = f (f (f x))

s m f x = f (m f x)

p m n = m s (n s z)
p2 m n f x = m f (n f x)

ti m n = m . n


tru x y = x
fls x y = y

f .. g = \x -> \y -> f (g x y)

and a b = a b fls
or a b = a tru b
not b = b fls tru
nand = Main.not .. Main.and
nor = Main.not @# Main.or

pair x y b = b x y
fst p = p tru
snd p = p fls

empty = undefined
isEmpty = undefined
cons e l = pair e l
head lst = Main.fst lst
tail lst = Main.snd lst
