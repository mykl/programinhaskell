> module Main where 

> import Regex
> import Test.HUnit

> main = runTestTT tests

> tests = TestList $ [
>    assertAccepts "z" "z",
>    assertAccepts "a" "a",
>    assertAccepts "(D)" "D",
>    assertNotAccepts "a" "z"] ++
>    assertAcceptsAll "b*" ["","b","bb", "bbbbbbbbbbb"] ++
>    assertAcceptsAll "1|3" ["1", "3"]


> assertAccepts :: Pattern -> String -> Test
> assertAccepts pat str = TestLabel pat 
>    (TestCase (assertBool (pat ++ " accepts " ++ str) (accepts pat str)))

> assertNotAccepts :: Pattern -> String -> Test
> assertNotAccepts pat str = TestLabel pat
>    (TestCase (assertBool (pat ++ " doesn't accept " ++ str) (not (accepts pat str))))

> assertAcceptsAll :: Pattern -> [String] -> [Test]
> assertAcceptsAll pat ss = map (assertAccepts pat) ss
