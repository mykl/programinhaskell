import Parser hiding (digit)
import Prelude hiding ((>>=), return)

type Pattern = String

accepts :: Pattern -> String -> Bool
accepts pat cs = case parse p cs of
                   [] -> False
                   [(_,[])] -> True
                   [(_,_)] -> False
                 where p = compile pat
                 

compile :: String -> Parser String
compile cs = case parse orexpr cs of
                [] -> failure
                [(p,[])] -> p
                [(p,_)] -> failure

orexpr :: Parser (Parser String)
orexpr = concatexpr >>= \ce ->
         (
            char '|' >>= \_ ->
            orexpr >>= \oe ->
            return $ ce +++ oe
         ) +++ return ce

-- lift the function f up to work on parsers
liftP2 :: (a1->a2->r)->Parser a1->Parser a2->Parser r
liftP2 f p q = p >>= \v1 ->
               q >>= \v2 ->
               return (f v1 v2)

concatexpr :: Parser (Parser String)
concatexpr = repeatexpr >>= \re ->
             (
                concatexpr >>= \ce ->
                return $ liftP2 (++) re ce
             ) +++ return re


-- Parser Map (like map but for parsers not list)
pmap :: (a -> b) -> Parser a -> Parser b
pmap f p = (\inp -> case parse p inp of
                 [] -> []
                 [(v,out)] -> [(f v,out)])

repeatexpr :: Parser (Parser String)
repeatexpr = basicexpr >>= \p ->
             (
               char '*'>>= \_ ->
               return $ pmap concat (many p)
             ) +++ return p

basicexpr :: Parser (Parser String)
basicexpr = parenexpr +++ charexpr

parenexpr :: Parser (Parser String)
parenexpr = char '(' >>= \_ ->
            orexpr >>= \charParser ->
            char ')' >>= \_ ->
            return charParser

charexpr :: Parser (Parser String)
charexpr = (regchar >>= \c ->
           return (string [c])) +++ 
           (escapechar >>= \c ->
           return (string [c]))

regchar :: Parser Char
regchar = item >>= \c ->
          if (c == '\\' || c == '|' || c == '(' || c == ')' 
              || c == '*') then failure
          else return c

escapechar :: Parser Char
escapechar = char '\\' >>= \_ ->
             item >>= \c ->
             if  (c == '\\' || c=='|' || c=='(' || c == ')' 
                 || c=='*') then return c
             else failure
