\documentclass[12pt]{article}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{courier}
\usepackage{color}
\usepackage{textcomp}
\usepackage{hyperref}
\usepackage{xspace}
\usepackage{amsmath}
\begin{document}

\lstset{language=Haskell}
\lstset{upquote=true}
\lstset{basicstyle=\footnotesize\ttfamily,keywordstyle=\color{blue},extendedchars=true}
\newcommand{\inlcsharp}{\lstinline[breaklines=true,language={[Sharp]C},keywordstyle=\ttfamily]}
\newcommand{\inlhaskell}{\lstinline[breaklines=true,language=Haskell,keywordstyle=\ttfamily]}
\newcommand{\csharp}{C\#\xspace}
\newcommand{\problem}{\textbf{Problem:}\xspace}
\newcommand{\solution}{\textbf{Solution:}\xspace}



\section*{Chapter 6 Bonus Question}

\problem Write a Haskell function 
\inlhaskell=toCFract :: Double -> [Int]= to convert a floating 
point number to its continued fraction representation as a list of integers.
Hint: I needed to use the Haskell functions \inlhaskell=floor= and
\inlhaskell=fromInteger= to do this assignment. 

Example:
\begin{lstlisting}
> toCFract 1.25
[1,4]
> toCFract (-3.25)
[-4,1,3,1125899906842624]*
\end{lstlisting}
* The actual answer is \inlhaskell=[-4,1,3]=. The large number at the
end is due to rounding error. Don't worry if your results contain
rounding errors like this.

I'll explain the algorithm in this paper. For more information see
\url{http://en.wikipedia.org/wiki/Continued_fraction}.

You may notice that the wikipedia site uses a semicolon after the first
number (like $[1;2,3]$). Don't worry about that for now. We are using
lists to represent continued fractions, and they use the comma for a separator.

\subsection*{Algorithm}
Here is the algorithm for \inlhaskell=toCFract r=:
\begin{itemize}
\item If $r$ is zero then return \inlhaskell=[0]= and end.

\item Otherwise:
    \begin{enumerate}
    \item Let $i$ be the integral part (floor) of $r$. 
    \item Let $f$ be the fractional part of $r$: $r - i$.
    \item Let $is$ be the continued fraction of $1/f$ (iff $f \ne 0$). If
          $f = 0$ then don't calculate $is$.
    \item If $f = 0 \vee is = [0]$ return $[i]$ else return $i : is$
    \end{enumerate}
\end{itemize}
Note: $\vee$ means ``or''.

There is a good example of doing this manually at wikipedia:
\href{http://en.wikipedia.org/wiki/Continued_fraction#Calculating_continued_fraction_representations}{Calculating Continued Fractions}.

\subsection*{Reverse Algorithm}
It might be easier to see what is going on if we examine how to convert
a continued fraction into a rational number.
A continued fraction is written as a list of integers 
$[a_0, a_1, a_2, \ldots, a_n]$. The first number, $a_0$, can be negative or
zero. Every other integer must be greater than 0.
Here is the formula to calculate the rational
number from this list:
\[
a_0 + \cfrac{1}{a_1 + \cfrac{1}{a_2 + \cfrac{1}{a_3 + \ddots}}}
\]

Here are some examples:\\
$[1,4] = 1 + \cfrac{1}{4} = 1.25$\\
$[-4,1,3] = -4 + \cfrac{1}{1 + \cfrac{1}{3}} = -3.25$

\subsection*{Examples}
Here is an example of calculating the continued fraction of $\sqrt{2}$.
It can even be done without a calculator as long as you can approximate
$\sqrt{2}$ in your head. You just need to know it's between 1 and 2.

\begin{tabular}{ccccc}
Real Num     & Integer Part & Fractional Part & Reciprocal               & 
                                                                Simplified \\

$\sqrt{2}$   & $1$          & $\sqrt{2}-1$    & $\dfrac{1}{\sqrt{2}-1}$  & 
                                                               $\sqrt{2}+1$\\

$\sqrt{2}+1$ & $2$          & $\sqrt{2}-1$    &  $\dfrac{1}{\sqrt{2}-1}$ & 
                                                               $\sqrt{2}+1$\\

\end{tabular}

It's obvious this will repeat and the continued fraction representation of
$\sqrt{2}$ is $[1,2,2,2,2,\ldots]$. (You can try this with $\sqrt{3}, \sqrt{5},
\ldots$ as well. You don't need a calculator for any of them.)

And another exampe with $r=64/27$.

\begin{tabular}{ccccc}
Real Num     & Integer Part & Fractional Part & Reciprocal               & 
                                                        Simplified \\

$2\dfrac{10}{27}$ & $2$     & $\dfrac{10}{27}$& $\dfrac{27}{10}$         &
                                                        $2 \dfrac{7}{10}$\\\\

$2 \dfrac{7}{10}$ & $2$     & $\dfrac{7}{10}$ & $\dfrac{10}{7}$          &
                                                        $1 \dfrac{3}{7}$\\\\

$1 \dfrac{3}{7}$  & $1$     & $\dfrac{3}{7}$  & $\dfrac{7}{3}$           &
                                                        $2 \dfrac{1}{3}$\\\\

$2 \dfrac{1}{3}$  & $2$     & $\dfrac{1}{3}$  & $3$      & $3$ \\\\

$3$               & $3$     & $0$             & STOP!    &   \\ 

\end{tabular}

Reading of the Integer column we get the result: $[2,2,1,2,3]$.


\subsection*{Further Information}

A continued fraction is a representation of a number that has several unique
properties. I'll just list a couple:

\begin{itemize}

\item Unique representation. Fractional notation gives us an infinite number
of ways of writing a number: (i.e. $1/2$, $2/4$, $3/6$ \ldots). Continued
fractions have one representation. (Actually, there are two possible 
representations for rational numbers, but by convention the shorter one
is always used.)

\item All rational numbers have a finite representation. Unlike decimal 
notation that requires an infinite representation for some numbers like
$1/3 = 0.3333\overline{3}$.

\item ``Truncated'' continued fractions for irrational numbers provide in some
sense the best possible rational approximation. The rational approximation of
$\pi$, $22/7$, comes from its continued fraction. The first 5 terms of $\pi$
are $[3;7,15,1,292]$. The fraction $22/7 \approx 3.1429$ comes from just using
the first 2 terms. Adding just two more terms gives a very good approximation of
$355/113 \approx 3.1415929$. The real value of $\pi = 3.14159265 \ldots$.
\end{itemize}



\end{document}
