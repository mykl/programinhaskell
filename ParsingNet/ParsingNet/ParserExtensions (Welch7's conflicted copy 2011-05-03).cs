
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
namespace ParsingNet
{
    using P = Parsers;

    public static class ParserExtensions
    {
        public static Parser<T> AsParser<T>(this T value)
		{
            if (value == null) throw new ArgumentException("Right here");
            return input => ParseResult<T>.Success(value, input);
		}
		
        public static ParseResult<T> Parse<T>(this Parser<T> parser, string input)
        {
            return parser(input);
        }



        public static Parser<T> OrElse<T>(this Parser<T> firstChoice, Parser<T> secondChoice)
        {
            return input =>
            {
                Debug.WriteLine(string.Format("OrElse: {0}", input));
                ParseResult<T> result = firstChoice(input);
                return result.Any() ? result : secondChoice(input);
            };
			
        }
		

        public static bool IsEmpty<T>(this ParseResult<T> result)
        {
            return !result.Any();
        }

        // this is the same as liftM in Haskell
        public static Parser<TResult> Select<T, TResult>(this Parser<T> parser, Func<T, TResult> func)
        {
            return input =>
            {
                Debug.WriteLine(string.Format("Select {0}", input));
                var result2 = parser.Invoke(input);
                if (!result2.Any())
                {
                    return ParseResult<TResult>.Empty;
                }
                var value = result2.First().Value;
                var output = result2.First().Remaining;

                return ParseResult<TResult>.Success(func(value), output);
            };

        }

        // this is the same as the bind operator >>= in Haskell
        public static Parser<T2> SelectMany<T1, T2>(this Parser<T1> p1, Func<T1, Parser<T2>> func)
        {
            return input =>
            {
                Debug.WriteLine(string.Format("SelectMany2: {0}", input));
                var result2 = p1.Invoke(input);
                if (!result2.Any())
                {
                    return ParseResult<T2>.Empty;
                }
                var value = result2.First().Value;
                var output = result2.First().Remaining;
                return func(value).Invoke(output);
            };


        }

        // this is like the bind operator >>= and the unit function (return) in Haskell Monads
        public static Parser<TResult> SelectMany<T1, T2, TResult>(this Parser<T1> p1, Func<T1, Parser<T2>> func, Func<T1, T2, TResult> func2)
        {
            return input =>
            {
                Debug.WriteLine(string.Format("SelectMany3: {0}", input));
                var result2 = p1.Invoke(input);
                if (!result2.Any())
                {
                    return ParseResult<TResult>.Empty;
                }
                var value = result2.First().Value;
                var output = result2.First().Remaining;

                var result3 = func(value).Invoke(output);

                if (!result3.Any())
                {
                    return ParseResult<TResult>.Empty;
                }

                var value2 = result3.First().Value;
                var output2 = result3.First().Remaining;

                return ParseResult<TResult>.Success(func2(value, value2), output2);

            };
        }


        public static Parser<T> Where<T>(this Parser<T> parser, Func<T, bool> predicate)
        {
            return input =>
            {
                Debug.WriteLine(string.Format("Where: {0}", input));
                var next = parser.Invoke(input);
                if (!next.Any()) return ParseResult<T>.Empty;
                var value = next.First().Value;
                var output = next.First().Remaining;

                if (predicate(value)) return ParseResult<T>.Success(value, output);
                return ParseResult<T>.Empty;
            };

        }

        // this is the same as liftM2 in Haskell
        public static Parser<TResult> Select<T1, T2, TResult>(this Parser<T1> p1, Parser<T2> p2, Func<T1, T2, TResult> func)
        {
            return from s1 in p1
                   from s2 in p2
                   select func(s1, s2);
        }

        public static Parser<TResult> Select<T1, T2, T3, TResult>(this Parser<T1> p1, Parser<T2> p2, Parser<T3> p3, Func<T1, T2, T3, TResult> func)
        {
            return from s1 in p1
                   from s2 in p2
                   from s3 in p3
                   select func(s1, s2, s3);
        }

        public static Parser<T2> Apply<T1, T2>(this Parser<Func<T1, T2>> funcParser, Parser<T1> p1)
        {
            var result = from func in funcParser
                         from t in p1
                         select func(t);
            return result;
        }

        public static Parser<T> Token<T>(this Parser<T> parser)
        {

            return from spaces1 in P.WhiteSpace
                   from token in parser
                   from spaces2 in P.WhiteSpace
                   select token;

        }

        public static Parser<IEnumerable<T>> Repeat<T>(this Parser<T> p)
        {
            return RepeatOneOrMore(p).OrElse(Enumerable.Empty<T>().AsParser());
        }



        public static Parser<IEnumerable<T>> RepeatOneOrMore<T>(this Parser<T> p)
        {
            var result = from v in p
                         from vs in Repeat(p)
                         select v.ToList().Concat(vs);


            return result;
        }

        // this is the same as the join method on Haskell monads
        public static Parser<T> Join<T>(this Parser<Parser<T>> parser)
        {
            return parser.SelectMany(Identity);
        }


        private static T Identity<T>(T input)
        {
            return input;
        }
    }
}
