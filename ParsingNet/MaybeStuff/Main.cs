﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaybeStuff
{
    public class Program
    {


        public static void Main(String[] args)
		{

            Optional<ObjectM> myObj = new ObjectM(new Attribute(5, 75));
            Optional<ObjectM> myObj2 = new ObjectM(new Attribute(5, 123));

            var result1 = from obj in myObj
                         from attr2 in obj.Attribute
                         from val in attr2.Value
                         where val < 100
                         select val * 3;

            var result2 = from obj in myObj2
                          from attr2 in obj.Attribute
                          from val in attr2.Value
                          where val > 100
                          select val * 2;

            var result = from r in result1 
                         from r2 in result2
                         select r + r2;


            Console.WriteLine(result);


        }

        public static void Main2()
        {
            Optional<Parent> parent1 = new Parent();
            var result = parent1.Cast<Child>();

            Optional<Child> child1 = new Child();
            var result2 = child1.Cast<Parent>();

        }


    }

    public class Parent
    {
        
    }

    public class Child : Parent
    {
        
    }

    public class Attribute
    {
        public Attribute(double value)
        {
            Status = Optional<int>.Nothing;
            Value = value;
        }
        public Attribute(int status, double value)
        {
            Status = status;
            Value = value;
        }
        public Optional<int> Status;
        public Optional<double> Value;
    }

    public class ObjectM
    {
        public ObjectM()
        {
            Attribute = Optional<Attribute>.Nothing;
        }
        public ObjectM(Attribute attribute)
        {
            Attribute = attribute;
        }

        public readonly Optional<Attribute> Attribute;
    }

}
