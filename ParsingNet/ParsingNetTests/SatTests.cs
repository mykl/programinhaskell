﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;
    [TestClass]
    public class SatTests 
    {
        [TestMethod]
        public void CharMatches()
        {
            Assert.AreEqual(P.Char('c').Parse("chair"), ParseResult<char>.Success('c', "hair"));
        }

        [TestMethod]
        public void CharNoMatch()
        {
            Assert.AreEqual(P.Char('c').Parse("abcde"), ParseResult<char>.Empty);
        }

        [TestMethod]
        public void CharWithEmptyString()
        {
            Assert.AreEqual(P.Char('d').Parse(""), ParseResult<char>.Empty);
        }

        [TestMethod]
        public void CharWithNullString()
        {
            Assert.AreEqual(P.Char('e').Parse(null), ParseResult<char>.Empty);
        }

        [TestMethod]
        public void LowerTests()
        {
            Assert.AreEqual(P.Lower.Parse("lower"), ParseResult<char>.Success('l', "ower"));
            Assert.AreEqual(P.Lower.Parse("Lower"), ParseResult<char>.Empty);
        }

        [TestMethod]
        public void StringTests()
        {
            Assert.AreEqual(P.String("hello").Parse("hello, there"), ParseResult<string>.Success("hello", ", there"));
        }
    }
}
