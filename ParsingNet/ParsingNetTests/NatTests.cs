﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using TestClassAttribute=NUnit.Framework.TestFixtureAttribute;
using TestMethodAttribute=NUnit.Framework.TestAttribute;

namespace ParsingNet
{
    using P = Parser;
    [TestClass]
    public class NatTests 
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(P.Nat.Parse("3"), ParseResult<ulong>.Success(3, ""));
        }
    }
}
