module Ex6 where
import MyParser
import Ex5 
import Prelude hiding (return,(>>=))

expr :: Parser Prop
expr = pEquiv

pEquiv :: Parser Prop
pEquiv =  pImply >>= \p ->
              (symbol "<->" >>= \_ ->
               pEquiv >>= \q ->
               return $ Equiv p q)
              +++ return p

pImply :: Parser Prop
pImply = pOr >>= \p ->
             (symbol "->" >>= \_ ->
              pImply >>= \q ->
              return $ Imply p q)
             +++ return p

pOr :: Parser Prop
pOr = pAnd >>= \p ->
          (symbol "v" >>= \_ ->
           pOr >>= \q ->
           return $ Or p q)
          +++ return p

pAnd :: Parser Prop
pAnd = basicExpr >>= \p ->
           (symbol "^" >>= \_ ->
            pAnd >>= \q ->
            return $ And p q)
           +++ return p

basicExpr :: Parser Prop
basicExpr = pConst +++ pVar +++ parenExpr +++ pNot

parenExpr :: Parser Prop
parenExpr = symbol "(" >>= \_ ->
            expr >>= \p ->
            symbol ")" >>= \_ ->
            return p

pNot :: Parser Prop
pNot = (symbol "~" >>= \_ ->
        basicExpr >>= \p ->
        return $ Not p)


pConst :: Parser Prop
pConst = (symbol "True" >>= \_ ->
          return $ Const True)
         +++
         (symbol "False" >>= \_ ->
          return $ Const False)

pVar :: Parser Prop
pVar = upper >>= \c ->
       return $ Var c

