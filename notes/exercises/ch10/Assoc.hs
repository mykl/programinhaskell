module Assoc(Assoc,createAssoc,find) where

data Assoc k v = Assoc [(k,v)]

createAssoc :: [(k,v)] -> Assoc k v
createAssoc kvps = Assoc kvps

find :: Eq k => k -> Assoc k v -> v
find k (Assoc kvps) = head [ v | (k',v) <- kvps, k==k' ]
