import Tree 

-- This version is more efficient than the one in the bok
-- because it only needs to do one comparison of n and m
-- (Of course it still needs to do pattern matching on 
--  the result with LT, EQ, and GT).
occurs :: Ord a => a -> Tree a -> Bool
occurs n (Leaf m) = n == m
occurs n (Node l m r) = case compare n m of
    LT -> occurs n l
    EQ -> True
    GT -> occurs n r

