module Ex5 where
import Assoc

data Prop = Const Bool
    | Var Char
    | Not Prop
    | And Prop Prop
    | Imply Prop Prop
    | Or Prop Prop
    | Equiv Prop Prop

instance Show Prop where
    show (Const b) = show b
    show (Var c) = c:[]
    show (Not p) = "~" ++ show p
    show (And p q) = showBinaryRel "^" p q
    show (Imply p q) = showBinaryRel "->" p q
    show (Or p q) = showBinaryRel "v" p q
    show (Equiv p q) = showBinaryRel "<->" p q

showBinaryRel :: String -> Prop -> Prop -> String
showBinaryRel op p q = "(" ++ show p ++ " " ++ op ++ " " ++ show q ++ ")"

type Subst = Assoc Char Bool

eval :: Subst -> Prop -> Bool
eval _ (Const b) = b
eval s (Var c) = find c s
eval s (Not p) = not (eval s p)
eval s (And p q) = eval s p && eval s q
eval s (Imply p q) = eval s p <= eval s q
eval s (Or p q) = eval s p || eval s q
eval s (Equiv p q) = eval s p == eval s q

vars :: Prop -> [Char]
vars (Const _) = []
vars (Var c) = [c]
vars (Not p) = vars p
vars (And p q) = vars p ++ vars q
vars (Imply p q) = vars p ++ vars q
vars (Or p q) = vars p ++ vars q
vars (Equiv p q) = vars p ++ vars q

rmdups :: Eq a => [a] -> [a]
rmdups [] = []
rmdups (x:xs) = x : rmdups (filter (/= x) xs)

distinctVars :: Prop -> [Char]
distinctVars = rmdups . vars

bools :: Int -> [[Bool]]
bools 0 = [[]]
bools n = [ False : bs | bs <- bss ] ++ [ True : bs | bs <- bss ]
    where bss = bools (n-1)

isTaut :: Prop -> Bool
isTaut p = and [ eval (createAssoc s) p | s <- ss ]
    where ss = map (zip varsp) (bools (length varsp))
          varsp = vars p
