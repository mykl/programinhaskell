-- Monad Maybe
-- Monad []
import Prelude hiding (Just, Nothing, Maybe )

data Maybe a = Nothing | Just a

instance Monad Maybe where
    m >>= f = case m of
                  Nothing -> Nothing
                  Just n -> f n
    return v = Just v

data List a = Nil | Cons a (List a) deriving(Show)

append :: List a -> List a -> List a
append xs Nil = xs
append Nil ys = ys
append (Cons x xs) ys = Cons x (append xs ys)

l2list :: [a] -> List a
l2list [] = Nil
l2list (x:xs) = Cons x (l2list xs)

lfoldr :: (a -> b -> b) -> b -> (List a) -> b
lfoldr _ v Nil = v
lfoldr f v (Cons x xs) = f x (lfoldr f v xs)

instance Monad List where
    l >>= f = lfoldr (append . f) Nil l
    return v = Cons v Nil
