import Prelude hiding (map,iterate)
type Bit = Int

unfold :: (t -> Bool) -> (t -> a) -> (t -> t) -> t -> [a]
unfold p h t x | p x =       []
               | otherwise = h x : unfold p h t (t x)

chop8 :: [Bit] -> [[Bit]]
chop8 = unfold null (take 8) (drop 8)

map :: (a -> b) -> [a] -> [b]
map f = unfold null (f . head) tail

-- Explanation of use of unfold.
-- iterate f is a function that takes one argument.
-- pred: (const False) produces an infinite list since 
--       it never is True.
-- head: whatever is passed to iterate f, 
         we use that as the head of the list
-- tail: whatever is passed to iterate f, 
         we apply f to it for the tail
iterate :: (a -> a) -> a -> [a]
iterate f = unfold (const False) id f
