import Prelude hiding (and, concat, replicate, (!!), elem)
and :: [Bool] -> Bool
and [] = True
and (True:xs) = and xs
and (False:xs) = False

concat :: [[a]] -> [a]
concat [] = []
concat ([]:xss) = concat xss 
concat ((x:xs):xss) = x : concat (xs:xss)

replicate :: Int -> a -> [a]
replicate 0 _ = []
replicate n x = x : replicate (n-1) x

(!!) :: [a] -> Int -> a
(x:xs) !! 0 = x
(_:xs) !! n = xs !! (n-1)

elem :: Eq a => a -> [a] -> Bool
elem _ [] = False
elem y (x:xs) | x == y = True
              | otherwise = elem y xs

