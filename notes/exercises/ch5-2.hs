-- Create a list of n identical elements e
replicate :: Int -> a -> [a]
replicate n e = [ e | _ <- [1..n]]
