import MyParser
import Prelude hiding ((>>=), return)

eol :: Parser Char
eol = char '\n'

notchar :: Char -> Parser ()
notchar c = item >>= \c' -> 
            if (c == c') then failure else return ()

comment :: Parser ()
comment = (symbol "--") >>= \_ ->
          many (notchar '\n') >>= \_ ->
          eol >>= \_ ->
          return ()
