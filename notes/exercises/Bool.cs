namespace Welch.Booleans
{
    public abstract class Bool
    {
        // Define legal values of Bool
        public static readonly Bool True = new TrueType();
        public static readonly Bool False = new FalseType();

        public abstract Bool Not();
        public abstract Bool And(Bool b);
        public abstract Bool Or(Bool b);
        public abstract override string ToString();
        public abstract override bool Equals(object obj);
        public abstract override int GetHashCode();


        private sealed class TrueType : Bool
        {
            public override Bool Not()
            {
                return False;
            }

            public override Bool And(Bool b)
            {
                return b;
            }

            public override Bool Or(Bool b)
            {
                return True;
            }

            public override string ToString()
            {
                return "True";
            }

            public override bool Equals(object obj)
            {
                return True == obj;
            }

            public override int GetHashCode()
            {
                return 1;
            }
        }

        private sealed class FalseType : Bool
        {
            public override Bool Not()
            {
                return True;
            }

            public override Bool And(Bool b)
            {
                return False;
            }

            public override Bool Or(Bool b)
            {
                return b;
            }

            public override string ToString()
            {
                return "False";
            }

            public override bool Equals(object obj)
            {
                return False == obj;
            }

            public override int GetHashCode()
            {
                return 0;
            }
        }
    }
}

