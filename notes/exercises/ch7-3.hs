import Prelude hiding (map,filter)
-- redfine map f and filter p using foldr

map :: (a -> b) -> [a] -> [b]
map f = foldr (\x -> (f x :) ) []

-- Using a composition to eliminate lambda
map2 :: (a -> b) -> [a] -> [b]
map2 f = foldr ((:) . f) []

filter :: (a -> Bool) -> [a] -> [a]
filter p = foldr (\x -> if p x then (x :) else id) []
