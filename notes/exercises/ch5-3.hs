-- Find all pythagorean triples with components <= n
pyths :: Int -> [(Int, Int, Int)]
pyths n = [(x,y,z) | x<-is, y<-is, z<-is, x^2+y^2==z^2 ]
          where is = [1..n]
