{-# LANGUAGE CPP #-}
#include "ch7-8.hs"

transmit' :: String -> String
transmit' = decode . faultychannel . encode

faultychannel :: [Bit] -> [Bit]
faultychannel = tail
