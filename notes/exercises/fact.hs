fact :: Int -> Int
fact n = product [1..n]

recfact :: Int -> Int
recfact 0 = 1
recfact n = n * recfact (n-1)

