import Char
-- Caesar cipher variant 
-- Encodes upper and lower case letters.

-- Maps a - z to 0 - 25 and A - Z to 26 - 51
-- Any other character is an error.
let2int :: Char -> Int
let2int c | isLower c = ord c - ord 'a'
          | isUpper c = ord c - ord 'A' + 26

-- reverse of let2int
int2let :: Int -> Char
int2let n | n < 26 = chr (ord 'a' + n)
          | otherwise = chr (ord 'A' + n - 26)

-- Shifts the char c by n places. Should only be
-- called with upper or lower case letters.
shift' :: Int -> Char -> Char
shift' n c = int2let ((let2int c + n) `mod` 52)

-- Shifts upper and lower case letters n places,
-- returns all other characters as is.
shift :: Int -> Char -> Char
shift n c | isLower c || isUpper c = shift' n c
          | otherwise              = c


-- Encodes a phrase
encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs]
