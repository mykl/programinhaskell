merge :: Ord a => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge list1@(x:xs) list2@(y:ys)  
    | x <= y    = x : merge xs list2
    | otherwise = y : merge list1 ys

-- Note: list1 and list2 above are names for the 
-- specified patterns. It avoids having to 
-- re-calculate a cons
