import Data.Char

unfold :: (a -> Bool) -> (a -> b) -> (a -> a) -> a -> [b]
unfold p h t x | p x       = []
               | otherwise = h x : unfold p h t (t x)

type Bit = Int
int2bin :: Int -> [Bit]
int2bin = unfold (== 0)(`mod` 2)(`div` 2)

make8 :: [Bit] -> [Bit]
make8 bits = take 8 (bits ++ repeat 0)

addparity :: [Bit] -> [Bit]
addparity bits = if (even $ sum bits) 
      then (0 : bits) else (1 : bits)

encode :: String -> [Bit]
encode = concat . map (addparity . make8 . int2bin . ord)

chop9 :: [Bit] -> [[Bit]]
chop9 = unfold (null)(take 9)(drop 9)

checkparity :: [Bit] -> [Bit]
checkparity (0:xs) | even $ sum xs = xs
checkparity (1:xs) | odd $ sum xs = xs
checkparity _ = error "parity error"

bin2int :: [Bit] -> Int
bin2int = foldr (\x y -> x + 2 * y) 0
                  
decode :: [Bit] -> String
decode = map (chr . bin2int . checkparity) . chop9

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = id
