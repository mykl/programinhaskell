import MyParser
import Prelude hiding ((>>=),return)

int :: Parser Int
int = natural +++ ( (symbol "-") >>= \_ -> natural >>= (\n -> return (-n)))

